<?php 
	session_start();

	if( isset($_SESSION['session_expire']) && !empty($_SESSION['session_expire']) ) {
		/**/
		if( strtotime("+2 minutes") >= $_SESSION['session_expire'] )
		{
			session_unset();
		}
	}

	echo json_encode($_SESSION);
?>