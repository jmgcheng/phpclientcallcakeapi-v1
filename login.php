<?php
	/**/
	session_start();


	/**/
	include __DIR__.'/vendor/autoload.php';
	use GuzzleHttp\Client;


	/**/
	define('TOKEN_URL', 'http://cake-rest-s1.trusted-freelancer.com/api/users/token');

	/*
	$_POST['username'] = 'guest';
	$_POST['password'] = 'guest';
	print_r($_POST);
	exit();
	*/

	if( 	isset($_POST['username']) && !empty($_POST['username']) 
		&&	isset($_POST['password']) && !empty($_POST['password']) 
	) 
	{
		$a_result = [];

		$client = new \GuzzleHttp\Client();
		$res = $client->request('POST', TOKEN_URL, [
		    'headers' => [
		        'Accept'     	=> 'application/json',
		        'Content-Type'	=> 'application/json'
		    ],
		    'json' => [
		    	'username' => $_POST['username'],
		    	'password' => $_POST['password']
		    ],
		    'http_errors' => false
		]);

		$o_result = json_decode($res->getBody());

		if( isset($o_result->data->token) && !empty($o_result->data->token) )
		{
			$_SESSION['access_token'] = ( isset($o_result->data->token) && !empty($o_result->data->token) ) ? $o_result->data->token : '';
			/*header('Location: ' . "http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']));*/
			$a_result['access_token'] = $_SESSION['access_token'];
			$a_result['result'] = 'success';
			echo json_encode($a_result);
    		exit();
		}
		else
		{
			$s_emsg = ( isset($o_result->data->message) && !empty($o_result->data->message) ) ? $o_result->data->message : '';
			echo 'Failed: ' . $s_emsg;
			exit();
		}
		
	}
	else
	{
		echo 'Failed: username and password incomplete.';
		exit();
	}