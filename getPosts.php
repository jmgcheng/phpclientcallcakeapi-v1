<?php
	/**/
	session_start();


	/**/
	include __DIR__.'/vendor/autoload.php';
	use GuzzleHttp\Client;


	/**/
	define('ACCESS_TOKEN', (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) ? $_SESSION['access_token'] : '');
	define('POST_URL', 'http://cake-rest-s1.trusted-freelancer.com/api/posts?token=' . ACCESS_TOKEN);

	/*
	$_POST['username'] = 'guest';
	$_POST['password'] = 'guest';
	print_r($_POST);
	exit();
	*/

	/*print_r($_SESSION);
	exit();*/

	if(	isset($_SESSION['access_token']) && !empty($_SESSION['access_token']) ) {
		$a_result = [];

		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', POST_URL, [
		    'headers' => [
		        'Accept'     	=> 'application/json' //,
		        /*'Content-Type'	=> 'application/json'*/
		    ] ,
		    'http_errors' => false
		]);

		$o_result = json_decode($res->getBody());

		/*print_r($o_result);
		echo '<br/><br/><br/><br/>';*/

		if( isset($o_result->success) && !empty($o_result->success && $o_result->success == 1) 
		)
		{
			$a_result['data'] = $o_result->data;
			$a_result['result'] = 'success';
			echo json_encode($a_result);
    		exit();
		}
		else
		{

			$s_emsg = ( isset($o_result->message) && !empty($o_result->message) ) ? $o_result->message : '';
			echo 'Failed: ' . $s_emsg;
			exit();
		}
		
	}
	else
	{
		echo 'Failed: username and password incomplete.';
		exit();
	}