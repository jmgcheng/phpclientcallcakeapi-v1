<?php
	/**/
	session_start();


	/*$_SESSION['access_token'] = 'frrtahfcsbxrilts5amjcvyl2mjo3xexnnkfxgu8';
	$_POST['content'] = 'testing';*/


	/**/
	include __DIR__.'/vendor/autoload.php';
	use GuzzleHttp\Client;


	/**/
	define('ACCESS_TOKEN', (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) ? $_SESSION['access_token'] : '');
	define('ADD_POST_URL', 'http://cake-rest-s1.trusted-freelancer.com/api/posts?token='.ACCESS_TOKEN);


	/**/
	if( !isset($_SESSION['access_token']) ){
		echo 'Fail: Try to login first and get a token.';
    	exit();
	}

	/**/
	if( isset($_POST['content']) && !empty($_POST['content']) ){

		$client = new \GuzzleHttp\Client();
		$res = $client->request('POST', ADD_POST_URL, [
		    'headers' => [
		        'Accept'     	=> 'application/json',
		        'Content-Type'	=> 'application/json'
		    ],
		    'json' => [
				'title' => 'A new post: ' . strtotime('now'),
				'content' => $_POST['content']
		    ],
		    'http_errors' => false
		]);

		/*echo $res->getStatusCode();
		echo '<br>';
		echo $res->getBody();
		exit();*/
		
		$a_result = array(
			'result'	=> 'success',
			'strtotime'	=> strtotime('now')
		);
		echo json_encode($a_result);
    	exit();
	}
	else{
		echo 'else failed';
		exit();
	}

?>