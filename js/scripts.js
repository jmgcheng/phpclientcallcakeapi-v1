var app = angular.module('myApp', ['ngRoute', 'ngSanitize'])
	.service('sharedVar', function () {
	    return {};
	})
	.config(function($routeProvider){
		$routeProvider
			.when('/post-listings', {
				templateUrl: 'view-templates/post-listings.html', 
				controller: 'postListingsController'
			})
			.when('/post-add', {
				templateUrl: 'view-templates/post-add.html', 
				controller: 'postAddController'
			})
			.when('/login', {
				templateUrl: 'view-templates/login.html', 
				controller: 'loginController'
			})
			.otherwise({
				redirectTo: 'login'
			})
	})

	.controller('myController', ['$scope', '$http', 'sharedVar', function($scope, $http, sharedVar){
		console.log('myController');
		$scope.a_posts = [];
		/*$scope.s_access_token = '';*/
		$scope.sharedVar = sharedVar;


		/**/
		$scope.getSessions = function(){
			$http({
				method  	:'POST',
				url 		:'getSessions.php',
			})
			.then(
				function(res){
					console.log('res success');
					console.log(res);
					/*$scope.s_access_token = (res.data.access_token != undefined) ? res.data.access_token : '';*/
					$scope.sharedVar.s_access_token = (res.data.access_token != undefined) ? res.data.access_token : '';
					console.log('s_access_token' + $scope.sharedVar.s_access_token);
				},
				function(res){
					console.log('res fail');
					console.log(res);	
				}
			);
		};
		$scope.getSessions();

	}])
	.controller('loginController', ['$scope', '$http', '$location', 'sharedVar', function($scope, $http, $location, sharedVar){
		console.log('loginController');
		$scope.frmLoginState = '';

		$scope.login = function() {

			$scope.frmLoginState = 'sending';

			$http({
				method 	 	:'POST',
				url 		:'login.php',
				data: $.param({
					'username'		: $scope.frmLogin.username,
					'password'		: $scope.frmLogin.password
				}),
				headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
			})
			.then(
				function(res){
					console.log('res success');
					console.log(res);
					/*$scope.s_access_token = (res.data.access_token != undefined) ? res.data.access_token : '';*/
					$scope.sharedVar.s_access_token = (res.data.access_token != undefined) ? res.data.access_token : '';
					console.log('s_access_token' + $scope.sharedVar.s_access_token);
					$('#frmLogin .alert').hide();
					if(res.data.result == 'success'){
						$scope.frmLoginState = 'success';
						$location.path( "/post-listings" );
					}
					else {
						$scope.frmLoginState = 'fail';
					}
				},
				function(res){
					console.log('res fail');
					console.log(res);	
					$scope.frmLoginState = 'fail';
				}
			);

		};

	}])	
	.controller('postListingsController', ['$scope', '$http', '$location', 'sharedVar', function($scope, $http, $location, sharedVar){
		console.log('postListingsController');


		/**/
		$scope.getSessions();


		/**/
		$scope.getWpPosts = function(){
			$http({
				method  	:'GET',
				url 		:'getPosts.php'
			})
			.then(
				function(o_res){
					console.log('res success');
					console.log(o_res);
					$scope.a_posts = o_res.data.data;

					console.log('test');
					console.log($scope.a_posts);
					console.log('test');

				},
				function(o_res){
					console.log('res fail');
					console.log(o_res);	
				}
			);
		};
		if( $scope.sharedVar.s_access_token != '' )
		{
			$scope.getWpPosts();
		}
		else
		{
			console.log('i dont have token' + $scope.sharedVar.s_access_token);
		}

		


	}])
	.controller('postAddController', ['$scope', '$http', '$location', 'sharedVar', function($scope, $http, $location, sharedVar){
		console.log('postAddController');
		$scope.frmAddPostState = '';

		/**/
		$scope.getSessions();


		/**/
		if( $scope.sharedVar.s_access_token == '' )
		{ $location.path( "/post-listings" ); }


		/**/
		$scope.addPost = function(){

			/*alert($scope.frmAddPost.content);
			return false;*/

			$scope.frmAddPostState = 'sending';

			$http({
				method  : 'POST',
				url 		:'addPost.php',
				data: $.param({
					'content'		: $scope.frmAddPost.content
				}),
				headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
			})
			.then(
				function(res){
					console.log('res success');
					console.log(res);
					$('#frmAddPost .alert').hide();
					if(res.data.result == 'success'){
						console.log('refreshing post');
						$scope.frmAddPostState = 'success';
						$location.path( "/post-listings" );
					}
					else {
						$scope.frmAddPostState = 'fail';
					}
				},
				function(res){
					console.log('res fail');
					console.log(res);	
					$scope.frmAddPostState = 'fail';
				}
			);


		};


	}])
	;